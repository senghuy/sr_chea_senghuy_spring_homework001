package com.example.homework1;


import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.ArrayList;

@RestController
@RequestMapping("/api/v1")
public class Controller {

    ArrayList<Customer> customerslist = new ArrayList<>();
    public int autoid;

    public Controller() {
        customerslist.add(new Customer(1,"Meng","M",22,"KPC"));
        customerslist.add(new Customer(2,"Lisa","F",20,"KPS"));
        customerslist.add(new Customer(3,"Run","M",24,"PP"));
        customerslist.add(new Customer(4,"Ling","F",20,"PP"));
        autoid = customerslist.size();
    }
    @GetMapping("/customers")
    public ArrayList<Customer> getAllCustomer(){
        return customerslist;
    }

    @PostMapping("/customers/insert")
    public ResponseEntity<?> insertCustomer(@RequestBody SubCustomer subcustomer)
    {
        autoid++;
        customerslist.add(new Customer(autoid,subcustomer.getName(),subcustomer.getGender(),
                subcustomer.getAge(),subcustomer.getAddress()));
        return ResponseEntity.ok(new CustomerResponse<ArrayList<Customer>>(
                "This record was successfully created",
                customerslist,
                HttpStatus.OK,
                LocalDateTime.now()
        ));
    }

    @GetMapping("/customers/{id}")
    public ResponseEntity<?> getCustomerById(@PathVariable ("id") Integer customId){
        for (Customer custom :customerslist){
            if(custom.getId() == customId)
                return ResponseEntity.ok(new CustomerResponse<Customer>(
                        "This record has found sucessfully",
                        custom,
                        HttpStatus.OK,
                        LocalDateTime.now()
                ));
        }
        return new ResponseEntity<>( HttpStatus.NOT_FOUND);
    }
    @GetMapping("/customers/search")
    public ResponseEntity<?> findCustomerByName(@RequestParam String name){
        for(Customer custom : customerslist){
            if(custom.getName().equals(name)) {
                return ResponseEntity.ok(new CustomerResponse<Customer>(
                        "This record has found sucessfully",
                        custom,
                        HttpStatus.OK,
                        LocalDateTime.now()));
            }
        }
        return new ResponseEntity<>( HttpStatus.NOT_FOUND);
    }
    @PutMapping("/customers/update/{id}")
    public ResponseEntity<?> updateCustomerById(@PathVariable ("id") Integer customId
            ,@RequestBody SubCustomer subCustomer){
        for(Customer custom: customerslist){
            if(custom.getId() == customId) {
                custom.setName(subCustomer.getName());
                custom.setGender(subCustomer.getGender());
                custom.setAge(subCustomer.getAge());
                custom.setAddress(subCustomer.getAddress());

                return ResponseEntity.ok(new CustomerResponse<Customer>(
                        "You're update sucessfully",
                        custom,
                        HttpStatus.OK,
                        LocalDateTime.now()
                ));
            }
        }
        return  new ResponseEntity<>( HttpStatus.NOT_FOUND);
    }



    @DeleteMapping("/customers/delete/{id}")
    public ResponseEntity<?> deleteCustomerById(@PathVariable ("id") Integer customId) {
        for (Customer custom : customerslist) {
            if (custom.getId() == customId) {
                customerslist.remove(custom);
                return ResponseEntity.ok(new CustomerResponse<ArrayList<Customer>>(
                        "Congratulation your delete is successfully",
                        HttpStatus.OK,
                        LocalDateTime.now()
                ));
            }
        }
        return  new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
}
