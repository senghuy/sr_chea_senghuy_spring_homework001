package com.example.homework1;

import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;

public class CustomerResponse<T> {
    private String message;
    private T customer;
    private HttpStatus status;
    private LocalDateTime dateTime;

    public CustomerResponse(  String message, T customer, HttpStatus status,LocalDateTime dateTime) {
        this.message = message;
        this.customer = customer;
        this.status = status;
        this.dateTime = dateTime;
    }

    public CustomerResponse(String message, HttpStatus status, LocalDateTime dateTime) {
        this.dateTime = dateTime;
        this.status = status;
        this.message = message;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }

    public HttpStatus getStatus() {
        return status;
    }

    public void setStatus(HttpStatus status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getCustomer() {
        return customer;
    }

    public void setCustomer(T customer) {
        this.customer = customer;
    }
}
